if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Scope Base"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { TFA.AttachmentColors["-"], "Why the hell would you equip this?" }
ATTACHMENT.Icon = "" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "BASE"

ATTACHMENT.WeaponTable = {
}

local shadowborder = 256
local cd = {}
local myshad
local debugcv = GetConVar("cl_tfa_debug_rt")
local shadowcv = GetConVar("cl_tfa_3dscope_overlay")

function ATTACHMENT:GetScopeTarget(myself)
	local vel = myself:GetStat("VElements")
	if not vel then return end
	local siVel = vel[myself:GetStat("INS2_SightVElement")]
	if not siVel then return end
	local mdl = siVel.curmodel
	if not IsValid(mdl) then return end
	local att = mdl:GetAttachment(mdl:LookupAttachment("scope_align"))
	if not att then return end
	if not att.Pos then return end
	local origin = mdl:GetAttachment(mdl:LookupAttachment("scope_origin"))
	if not origin then return end
	if not origin.Pos then return end
	local t = hook.Run("CalcView",LocalPlayer(),LocalPlayer():EyePos(),LocalPlayer():EyeAngles() + LocalPlayer():GetViewPunchAngles(), LocalPlayer():GetFOV() )
	if t then
		cam.Start3D(t.origin or LocalPlayer():EyePos(),t.angles or LocalPlayer():EyeAngles())
	else
		cam.Start3D(LocalPlayer():EyePos(),LocalPlayer():EyeAngles() + LocalPlayer():GetViewPunchAngles())
	end
	local pos = att.Pos:ToScreen()
	local originPos = origin.Pos:ToScreen()
	cam.End3D()

	return pos, originPos
end

function ATTACHMENT:GetMuzzleTarget(myself)
	if not myself.GetMuzzlePos then return end
	local att = myself:GetMuzzlePos()
	if not att then return end
	cam.Start3D(LocalPlayer():EyePos(),LocalPlayer():EyeAngles() + LocalPlayer():GetViewPunchAngles())
	local pos

	if isvector(att) then
		pos = att:ToScreen()
	elseif istable(att) then
		pos = att.Pos:ToScreen()
	end

	cam.End3D()

	return pos, pos
end

function ATTACHMENT:DistanceFactor(myself)
	local distfac
	local suffix = myself:GetStat("INS2_SightSuffix")
	if suffix and myself:GetStat("RTAttachment_" .. suffix) and myself:GetStat("ScopeDistance" .. suffix) and myself:GetStat("ScopeDistanceRange" .. suffix) then
		local targent = myself:IsFirstPerson() and myself.OwnerViewModel or myself
		if IsValid(targent) then
			local att = targent:GetAttachment(myself:GetStat("RTAttachment_Mosin"))
			if att and att.Pos then
				local dist = att.Pos:Distance(myself:GetOwner():GetShootPos() )
				distfac = 1 - 0.5 * math.Clamp( dist - myself:GetStat("ScopeDistanceMin" .. suffix),0,myself:GetStat("ScopeDistanceRange" .. suffix)) / myself:GetStat("ScopeDistanceRange" .. suffix)
			end
		end
	else
		distfac = (myself.CLIronSightsProgress or 1) * 0.4 + 0.5
	end
	return distfac
end

local tsFinal = {}
function ATTACHMENT:Attach(wep)
	if not IsValid(wep) then return end
	wep.RTCodeOld = wep.RTCodeOld or wep.RTCode

	wep.RTCode = function(myself, rt, scrw, scrh)
		if not IsValid(myself.Owner) then return end
		local rttw, rtth
		rttw = ScrW()
		rtth = ScrH()

		if not myshad then
			myshad = Material("vgui/scope_shadowmask")
		end

		local ang = IsValid(myself.OwnerViewModel) and myself.OwnerViewModel:GetAngles() or myself.Owner:EyeAngles()

		if wep.ScopeAngleTransforms_Mosin then
			ang:RotateAroundAxis(ang:Right(), wep.ScopeAngleTransforms_Mosin.p)
			ang:RotateAroundAxis(ang:Up(), wep.ScopeAngleTransforms_Mosin.y)
			ang:RotateAroundAxis(ang:Forward(), wep.ScopeAngleTransforms_Mosin.r)
		end

		cd.angles = ang
		cd.origin = myself.Owner:GetShootPos()
		cd.x = 0
		cd.y = 0
		cd.w = rttw
		cd.h = rtth
		cd.fov = wep:GetStat("RTScopeFOV")
		cd.drawviewmodel = false
		cd.drawhud = false
		render.Clear(0, 0, 0, 255, true, true)

		if myself.CLIronSightsProgress > 0.005 then
			render.RenderView(cd)
		end

		local ts, ts2 = self:GetScopeTarget(myself)

		if not ts then
			ts, ts2 = self:GetMuzzleTarget(myself)
		end

		cam.Start2D()

		if ts and shadowcv and shadowcv:GetBool() then
			local distfac = self:DistanceFactor(myself)
			if debugcv and debugcv:GetBool() then
				distfac = 1
			end
			ts.x = ts.x / scrw
			ts.y = ts.y / scrh
			ts2.x = ts2.x / scrw
			ts2.y = ts2.y / scrh
			tsFinal.x = ts.x + 0.5 - ts2.x
			tsFinal.y = ts.y + 0.5 - ts2.y
			tsFinal.x = tsFinal.x * rttw
			tsFinal.y = tsFinal.y * rttw
			local texW = rttw * distfac
			local texH = rtth * distfac
			local texX = tsFinal.x - texW / 2
			local texY = tsFinal.y - texH / 2
			surface.SetMaterial(myshad)
			surface.SetDrawColor(color_white)
			surface.DrawTexturedRect(texX, texY, texW, texH)
			surface.SetDrawColor(color_black)
			surface.DrawRect(0, 0, texX + 1, rtth)
			surface.DrawRect(texX + texW - 1, 0, rttw - texX - texW + 1, rtth)
			surface.DrawRect(0, 0, rttw, texY + 1)
			surface.DrawRect(0, texY + texH - 1, rttw, rtth - texY - texH + 1)
		else
			surface.SetMaterial(myshad)
			surface.SetDrawColor(color_white)
			surface.DrawTexturedRect(-shadowborder, -shadowborder, shadowborder * 2 + rttw, shadowborder * 2 + rtth)
		end

		surface.SetDrawColor(ColorAlpha(color_black, 255 * (1 - myself.CLIronSightsProgress)))
		surface.DrawRect(0, 0, scrw, scrh)
		cam.End2D()
	end
end

function ATTACHMENT:Detach(wep)
	if not IsValid(wep) then return end
	wep.RTCode = wep.RTCodeOld
	wep.RTCodeOld = nil
end

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end