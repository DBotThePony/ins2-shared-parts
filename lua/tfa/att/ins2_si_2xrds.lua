if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Aimpoint CompM2 2X"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { TFA.AttachmentColors["="], "2x zoom", TFA.AttachmentColors["-"], "10% higher zoom time",  TFA.AttachmentColors["-"], "5% slower aimed walking" }
ATTACHMENT.Icon = "entities/ins2_si_2xrds.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "2XRDS"
ATTACHMENT.Base = "ins2_scope_base"
ATTACHMENT.WeaponTable = {
	["VElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_2xrds"] = {
			["active"] = function(wep, val) TFA.INS2.AnimateSight(wep) return true end,
			["ins2_sightanim_idle"] = "4x_idle",
			["ins2_sightanim_iron"] = "4x_zoom",
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["WElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_2xrds"] = {
			["active"] = true
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["IronSightsPos"] = function( wep, val ) return wep.IronSightsPos_2XRDS or val end,
	["IronSightsAng"] = function( wep, val ) return wep.IronSightsAng_2XRDS or val end,
	["IronSightsSensitivity"] = function(wep,val)
		local res = val * wep:Get3DSensitivity( )
		return res, false, true
	end ,
	["Secondary"] = {
		["IronFOV"] = function( wep, val ) return wep.Secondary.IronFOV_2XRDS or val * 0.9 end,
		["ScopeZoom"] = function( wep, val ) return 2 end
	},
	["IronSightTime"] = function( wep, val ) return val * 1.10 end,
	["IronSightMoveSpeed"] = function(stat) return stat * 0.95 end,
	["RTScopeFOV"] = function(wep, val) return (wep.RTScopeFOV_2XRDS or wep:GetStat("Secondary.IronFOV")) / wep:GetStat("Secondary.ScopeZoom") / 2 end,
	["RTOpaque"] = -1,
	["RTMaterialOverride"] = -1,
	["INS2_SightVElement"] = "scope_2xrds",
	["INS2_SightSuffix"] = "2XRDS"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end