if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "7X Scope"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = {TFA.AttachmentColors["="], "7x zoom", TFA.AttachmentColors["-"], "30% higher zoom time", TFA.AttachmentColors["-"], "10% slower aimed walking"}
ATTACHMENT.Icon = "entities/ins2_si_mosin.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "MOSN"
ATTACHMENT.Base = "ins2_scope_base"
ATTACHMENT.WeaponTable = {
	["VElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_mosin"] = {
			["active"] = function(wep, val)
				TFA.INS2.AnimateSight(wep)

				return true
			end,
			["ins2_sightanim_idle"] = "scope_idle",
			["ins2_sightanim_iron"] = "scope_zoom"
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["WElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_mosin"] = {
			["active"] = true
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["IronSightsPos"] = function(wep, val) return wep.IronSightsPos_Mosin or val end,
	["IronSightsAng"] = function(wep, val) return wep.IronSightsAng_Mosin or val end,
	["IronSightsSensitivity"] = function(wep, val)
		local res = val * wep:Get3DSensitivity()

		return res, false, true
	end,
	["Secondary"] = {
		["IronFOV"] = function(wep, val) return wep.Secondary.IronFOV_Mosin or val * 0.9 end,
		["ScopeZoom"] = function(wep, val) return 7 end
	},
	["IronSightTime"] = function(wep, val) return val * 1.30 end,
	["IronSightMoveSpeed"] = function(stat) return stat * 0.9 end,
	["RTScopeFOV"] = function(wep, val) return (wep.RTScopeFOV_Mosin or wep:GetStat("Secondary.IronFOV")) / wep:GetStat("Secondary.ScopeZoom") / 2 end,
	["RTOpaque"] = -1,
	["RTMaterialOverride"] = -1,
	["INS2_SightVElement"] = "scope_mosin",
	["INS2_SightSuffix"] = "Mosin"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end