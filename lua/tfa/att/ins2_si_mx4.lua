if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "MX4 Scope"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { TFA.AttachmentColors["="], "8.7x zoom", TFA.AttachmentColors["-"], "40% higher zoom time",  TFA.AttachmentColors["-"], "10% slower aimed walking" }
ATTACHMENT.Icon = "entities/ins2_si_mx4.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "MX4"
ATTACHMENT.Base = "ins2_scope_base"
ATTACHMENT.WeaponTable = {
	["VElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_mx4"] = {
			["active"] = function(wep, val) TFA.INS2.AnimateSight(wep) return true end,
			["ins2_sightanim_idle"] = "scope_idle",
			["ins2_sightanim_iron"] = "scope_zoom",
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["WElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_mx4"] = {
			["active"] = true
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["IronSightsPos"] = function( wep, val ) return wep.IronSightsPos_MX4 or val end,
	["IronSightsAng"] = function( wep, val ) return wep.IronSightsAng_MX4 or val end,
	["IronSightsSensitivity"] = function(wep,val)
		local res = val * wep:Get3DSensitivity( )
		return res, false, true
	end ,
	["Secondary"] = {
		["IronFOV"] = function( wep, val ) return wep.Secondary.IronFOV_MX4 or val * 0.9 end,
		["ScopeZoom"] = function( wep, val ) return 8.7 end
	},
	["IronSightTime"] = function( wep, val ) return val * 1.40 end,
	["IronSightMoveSpeed"] = function(stat) return stat * 0.9 end,
	["RTScopeFOV"] = function(wep, val) return (wep.RTScopeFOV_MX4 or wep:GetStat("Secondary.IronFOV")) / wep:GetStat("Secondary.ScopeZoom") / 2 end,
	["RTOpaque"] = -1,
	["RTMaterialOverride"] = -1,
	["INS2_SightVElement"] = "scope_mx4",
	["INS2_SightSuffix"] = "MX4"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end