if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "PO 4x24P"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { TFA.AttachmentColors["="], "4x zoom", TFA.AttachmentColors["-"], "25% higher zoom time",  TFA.AttachmentColors["-"], "5% slower aimed walking" }
ATTACHMENT.Icon = "entities/ins2_si_po4x.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "PO4X"
ATTACHMENT.Base = "ins2_scope_base"
ATTACHMENT.WeaponTable = {
	["VElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_po4x"] = {
			["active"] = function(wep, val) TFA.INS2.AnimateSight(wep) return true end,
			["ins2_sightanim_idle"] = "po_idle",
			["ins2_sightanim_iron"] = "po_zoom",
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["WElements"] = {
		["rail_sights"] = {
			["active"] = true
		},
		["scope_po4x"] = {
			["active"] = true
		},
		["sights_folded"] = {
			["active"] = false
		}
	},
	["IronSightsPos"] = function( wep, val ) return wep.IronSightsPos_PO4X or val end,
	["IronSightsAng"] = function( wep, val ) return wep.IronSightsAng_PO4X or val end,
	["IronSightsSensitivity"] = function(wep,val)
		local res = val * wep:Get3DSensitivity( )
		return res, false
	end ,
	["Secondary"] = {
		["IronFOV"] = function( wep, val ) return wep.Secondary.IronFOV_PO4X or val * 0.9 end,
		["ScopeZoom"] = function( wep, val ) return 4 end
	},
	["IronSightTime"] = function( wep, val ) return val * 1.25 end,
	["IronSightMoveSpeed"] = function(stat) return stat * 0.95 end,
	["RTScopeFOV"] = function(wep, val) return (wep.RTScopeFOV_PO4X or wep:GetStat("Secondary.IronFOV")) / wep:GetStat("Secondary.ScopeZoom") / 2 end,
	["RTOpaque"] = -1,
	["RTMaterialOverride"] = -1,
	["INS2_SightVElement"] = "scope_po4x",
	["INS2_SightSuffix"] = "PO4X"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end