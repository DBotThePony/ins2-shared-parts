if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Laser"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { TFA.AttachmentColors["+"], "20% lower base spread", TFA.AttachmentColors["-"], "10% higher max spread" }
ATTACHMENT.Icon = "entities/ins2_att_ub_laser.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "LASR"

ATTACHMENT.WeaponTable = {
	["VElements"] = {
		["laser"] = {
			["active"] = true
		},
		["laser_beam"] = {
			["active"] = true
		}
	},
	["WElements"] = {
		["laser"] = {
			["active"] = true
		},
		["laser_beam"] = {
			["active"] = true
		}
	},
	["Primary"] = {
		["Spread"] = function(wep,stat) return math.max( stat * 0.8, stat - 0.01 ) end,
		["SpreadMultiplierMax"] = function(wep,stat) return stat * ( 1 / 0.8 ) * 1.1 end
	},
	["LaserSightAttachment"] = function(wep,stat) return wep.LaserSightModAttachment end,
	["LaserSightAttachmentWorld"] = function(wep,stat) return wep.LaserSightModAttachmentWorld or wep.LaserSightModAttachment end
}

sound.Add({
	name = "TFA_INS2_LaserSights",
	channel = CHAN_ITEM, -- for playing multiple times
	volume = 1,
	level = 60,
	pitch = {70, 150},
	sound = "weapons/ins2/att/laser_sights.ogg"
})

function ATTACHMENT:Attach(wep)
	local owner = wep:IsValid() and wep:GetOwner():IsValid() and wep:GetOwner() or wep

	if not IsValid(owner) then return end
	if math.random() > 0.2 then return end
	owner:EmitSound("TFA_INS2_LaserSights")
end

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end
